#/******************************************************************************
# * Icinga 2                                                                   *
# * Copyright (C) 2012-2017 Icinga Development Team (https://www.icinga.com/)  *
# *                                                                            *
# * This program is free software; you can redistribute it and/or              *
# * modify it under the terms of the GNU General Public License                *
# * as published by the Free Software Foundation; either version 2             *
# * of the License, or (at your option) any later version.                     *
# *                                                                            *
# * This program is distributed in the hope that it will be useful,            *
# * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
# * GNU General Public License for more details.                               *
# *                                                                            *
# * You should have received a copy of the GNU General Public License          *
# * along with this program; if not, write to the Free Software Foundation     *
# * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.             *
# ******************************************************************************/

%define plugindir %{_libdir}/nagios/plugins

%define icinga_user icinga
%define icinga_group icinga
%define icingacmd_group icingacmd

Summary:        Network monitoring application
Name:           icinga2
Version:        2.8.4
Release:        2%{?dist}
License:        GPL-2.0+
Url:            https://www.icinga.com/
Group:          System/Monitoring
Source:         https://github.com/Icinga/%{name}/archive/v%{version}.tar.gz
Requires:       %{name}-bin = %{version}-%{release}

%description
Meta package for Icinga 2 Core, DB IDO and Web.

%package bin
Summary:        Icinga 2 binaries and libraries
BuildRequires:  bison
BuildRequires:  cmake
BuildRequires:  flex
BuildRequires:  boost-devel
Requires:       systemd
Requires:       %{name}-libs = %{version}-%{release}

%description bin
Icinga 2 is a general-purpose network monitoring application.
This subpackage provides the binaries for Icinga 2 Core.

%package common
Summary:        Common Icinga 2 configuration
Requires(pre):  shadow-utils
Requires(post): shadow-utils
BuildRequires:  logrotate

%description common
This subpackage provides common directories, and the UID and GUID definitions
among Icinga 2 related packages.


%package doc
Summary:        Documentation for Icinga 2
Group:          Documentation/Other

%description doc
This subpackage provides documentation for Icinga 2.


%package libs
Summary:        Libraries for Icinga 2
Requires:       %{name}-common = %{version}-%{release}

%description libs
This subpackage provides the internal libraries for the daemon.


%package ido-mysql
Summary:        IDO MySQL database backend for Icinga 2
BuildRequires:  mysql-devel
Requires:       %{name} = %{version}-%{release}

%description ido-mysql
Icinga 2 IDO mysql database backend. Compatible with Icinga 1.x
IDOUtils schema >= 1.12


%package ido-pgsql
Summary:        IDO PostgreSQL database backend for Icinga 2
BuildRequires:  postgresql-devel >= 8.4
Requires:       %{name} = %{version}-%{release}

%description ido-pgsql
Icinga 2 IDO PostgreSQL database backend. Compatible with Icinga 1.x
IDOUtils schema >= 1.12

%global selinux_variants mls targeted
%global selinux_modulename %{name}

%package selinux
Summary:        SELinux policy module supporting icinga2
BuildRequires:  checkpolicy
BuildRequires:  hardlink
BuildRequires:  selinux-policy-devel
Requires:       %{name} = %{version}-%{release}
Requires(post):   policycoreutils-python
Requires(postun): policycoreutils-python

%description selinux
SELinux policy module supporting icinga2.

%package -n vim-icinga2
Summary:        Vim syntax highlighting for icinga2
Group:          Productivity/Text/Editors
Requires:       vim-filesystem

%description -n vim-icinga2
Provides Vim syntax highlighting for icinga2.


%package -n nano-icinga2
Summary:        Nano syntax highlighting for icinga2
Group:          Productivity/Text/Editors
Requires:       nano

%description -n nano-icinga2
Provides Nano syntax highlighting for icinga2.

%prep
%setup -q -n %{name}-%{version}

%build
CMAKE_OPTS="-DCMAKE_INSTALL_SYSCONFDIR=%{_sysconfdir} \
            -DCMAKE_INSTALL_LOCALSTATEDIR=%{_localstatedir} \
            -DCMAKE_BUILD_TYPE=RelWithDebInfo \
            -DUSE_SYSTEMD=ON \
            -DBoost_NO_BOOST_CMAKE=ON \
            -DICINGA2_LTO_BUILD=ON \
            -DICINGA2_PLUGINDIR=%{plugindir} \
            -DICINGA2_RUNDIR=%{_rundir} \
            -DICINGA2_SYSCONFIGFILE=/etc/sysconfig/icinga2 \
            -DICINGA2_USER=%{icinga_user} \
            -DICINGA2_GROUP=%{icinga_group} \
            -DICINGA2_COMMAND_GROUP=%{icingacmd_group}"
%if 0%{?fedora}
#CMAKE_OPTS="$CMAKE_OPTS -DICINGA2_WITH_STUDIO=true"
%endif

%{cmake} . $CMAKE_OPTS
%{make_build}

cd tools/selinux
for selinuxvariant in %{selinux_variants}
do
  make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile
  mv %{selinux_modulename}.pp %{selinux_modulename}.pp.${selinuxvariant}
  make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile clean
done
cd -


%install
%{make_install}

# install custom limits.conf for systemd
install -D -m 0644 etc/initsystem/icinga2.service.limits.conf %{buildroot}/etc/systemd/system/%{name}.service.d/limits.conf

# remove features-enabled symlinks
rm -f %{buildroot}/%{_sysconfdir}/%{name}/features-enabled/*.conf

cd tools/selinux
for selinuxvariant in %{selinux_variants}
do
  install -d %{buildroot}%{_datadir}/selinux/${selinuxvariant}
  install -p -m 644 %{selinux_modulename}.pp.${selinuxvariant} \
    %{buildroot}%{_datadir}/selinux/${selinuxvariant}/%{selinux_modulename}.pp
done
cd -

# TODO: Fix build problems on Icinga, see https://github.com/Icinga/puppet-icinga_build/issues/11
#/usr/sbin/hardlink -cv %%{buildroot}%%{_datadir}/selinux

install -D -m 0644 tools/syntax/vim/syntax/%{name}.vim %{buildroot}%{_datadir}/vim/vimfiles/syntax/%{name}.vim
install -D -m 0644 tools/syntax/vim/ftdetect/%{name}.vim %{buildroot}%{_datadir}/vim/vimfiles/ftdetect/%{name}.vim
install -D -m 0644 tools/syntax/nano/%{name}.nanorc %{buildroot}%{_datadir}/nano/%{name}.nanorc

%pre common
getent group %{icinga_group} >/dev/null || %{_sbindir}/groupadd -r %{icinga_group}
getent group %{icingacmd_group} >/dev/null || %{_sbindir}/groupadd -r %{icingacmd_group}
getent passwd %{icinga_user} >/dev/null || %{_sbindir}/useradd -c "icinga" -s /sbin/nologin -r -d %{_localstatedir}/spool/%{name} -G %{icingacmd_group} -g %{icinga_group} %{icinga_user}


%post common

%systemd_post %{name}.service

if [ ${1:-0} -eq 1 ]
then
	# initial installation, enable default features
	for feature in checker notification mainlog; do
		ln -sf ../features-available/${feature}.conf %{_sysconfdir}/%{name}/features-enabled/${feature}.conf
	done
fi

exit 0


%postun common


%systemd_postun_with_restart %{name}.service


if [ "$1" = "0" ]; then
	# deinstallation of the package - remove enabled features
	rm -rf %{_sysconfdir}/%{name}/features-enabled
fi

exit 0

%preun common
%systemd_preun %{name}.service
exit 0

%post ido-mysql
if [ ${1:-0} -eq 1 ]
then
	# initial installation, enable ido-mysql feature
	ln -sf ../features-available/ido-mysql.conf %{_sysconfdir}/%{name}/features-enabled/ido-mysql.conf
fi

exit 0

%postun ido-mysql
if [ "$1" = "0" ]; then
	# deinstallation of the package - remove feature
	rm -f %{_sysconfdir}/%{name}/features-enabled/ido-mysql.conf
fi

exit 0

%post ido-pgsql
if [ ${1:-0} -eq 1 ]
then
	# initial installation, enable ido-pgsql feature
	ln -sf ../features-available/ido-pgsql.conf %{_sysconfdir}/%{name}/features-enabled/ido-pgsql.conf
fi

exit 0

%postun ido-pgsql
if [ "$1" = "0" ]; then
	# deinstallation of the package - remove feature
	rm -f %{_sysconfdir}/%{name}/features-enabled/ido-pgsql.conf
fi

exit 0

%post selinux
for selinuxvariant in %{selinux_variants}
do
  /usr/sbin/semodule -s ${selinuxvariant} -i \
    %{_datadir}/selinux/${selinuxvariant}/%{selinux_modulename}.pp &> /dev/null || :
done
/sbin/fixfiles -R icinga2-bin restore &> /dev/null || :
/sbin/fixfiles -R icinga2-common restore &> /dev/null || :
/sbin/semanage port -a -t icinga2_port_t -p tcp 5665 &> /dev/null || :

%postun selinux
if [ $1 -eq 0 ] ; then
  /sbin/semanage port -d -t icinga2_port_t -p tcp 5665 &> /dev/null || :
  for selinuxvariant in %{selinux_variants}
  do
     /usr/sbin/semodule -s ${selinuxvariant} -r %{selinux_modulename} &> /dev/null || :
  done
  /sbin/fixfiles -R icinga2-bin restore &> /dev/null || :
  /sbin/fixfiles -R icinga2-common restore &> /dev/null || :
fi


%files
%doc COPYING

%files bin
%doc COPYING README.md NEWS AUTHORS CHANGELOG.md
%{_sbindir}/%{name}
%dir %{_libdir}/%{name}/sbin
%{_libdir}/%{name}/sbin/%{name}
%{plugindir}/check_nscp_api
%{_datadir}/%{name}
%exclude %{_datadir}/%{name}/include
%{_mandir}/man8/%{name}.8.gz

%files libs
%doc COPYING README.md NEWS AUTHORS CHANGELOG.md
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so*

%files common
%doc COPYING README.md NEWS AUTHORS CHANGELOG.md tools/syntax
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%{_sysconfdir}/bash_completion.d/%{name}
%attr(644,root,root) %{_unitdir}/%{name}.service
%dir /etc/systemd/system/%{name}.service.d
%attr(644,root,root) %config(noreplace) /etc/systemd/system/%{name}.service.d/limits.conf
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%attr(0750,root,%{icinga_group}) %dir %{_sysconfdir}/%{name}
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_sysconfdir}/%{name}/conf.d
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_sysconfdir}/%{name}/features-available
%exclude %{_sysconfdir}/%{name}/features-available/ido-*.conf
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_sysconfdir}/%{name}/features-enabled
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_sysconfdir}/%{name}/scripts
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_sysconfdir}/%{name}/zones.d
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/%{name}.conf
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/constants.conf
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/zones.conf
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/conf.d/*.conf
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/features-available/*.conf
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/zones.d/*
%config(noreplace) %{_sysconfdir}/%{name}/scripts/*
%dir %{_exec_prefix}/lib/%{name}
%{_exec_prefix}/lib/%{name}/prepare-dirs
%{_exec_prefix}/lib/%{name}/safe-reload
%attr(0750,%{icinga_user},%{icingacmd_group}) %{_localstatedir}/cache/%{name}
%attr(0750,%{icinga_user},%{icingacmd_group}) %dir %{_localstatedir}/log/%{name}
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_localstatedir}/log/%{name}/crash
%attr(0750,%{icinga_user},%{icingacmd_group}) %dir %{_localstatedir}/log/%{name}/compat
%attr(0750,%{icinga_user},%{icingacmd_group}) %dir %{_localstatedir}/log/%{name}/compat/archives
%attr(0750,%{icinga_user},%{icinga_group}) %{_localstatedir}/lib/%{name}
%attr(0750,%{icinga_user},%{icingacmd_group}) %ghost %dir %{_rundir}/%{name}
%attr(2750,%{icinga_user},%{icingacmd_group}) %ghost %{_rundir}/%{name}/cmd
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_localstatedir}/spool/%{name}
%attr(0770,%{icinga_user},%{icinga_group}) %dir %{_localstatedir}/spool/%{name}/perfdata
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_localstatedir}/spool/%{name}/tmp
%attr(0750,%{icinga_user},%{icinga_group}) %dir %{_datadir}/%{name}/include
%{_datadir}/%{name}/include/*

%files doc
%{_datadir}/doc/%{name}
%docdir %{_datadir}/doc/%{name}

%files ido-mysql
%doc COPYING README.md NEWS AUTHORS CHANGELOG.md
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/features-available/ido-mysql.conf
%{_datadir}/icinga2-ido-mysql

%files ido-pgsql
%doc COPYING README.md NEWS AUTHORS CHANGELOG.md
%config(noreplace) %attr(0640,%{icinga_user},%{icinga_group}) %{_sysconfdir}/%{name}/features-available/ido-pgsql.conf
%{_datadir}/icinga2-ido-pgsql

%files selinux
%doc tools/selinux/*
%{_datadir}/selinux/*/%{selinux_modulename}.pp

%files -n vim-icinga2
%{_datadir}/vim/vimfiles/syntax/%{name}.vim
%{_datadir}/vim/vimfiles/ftdetect/%{name}.vim

%files -n nano-icinga2
%{_datadir}/nano/%{name}.nanorc

%changelog
* Sat Jun 9 2018 spike <spike@fedoraproject.org> 2.8.4-2
- Spec file cleanup

* Sat Jun 9 2018 spike <spike@fedoraproject.org> 2.8.4-1
- Update to 2.8.4

* Tue Feb 06 2018 Markus Frosch <markus.frosch@icinga.com> 2.9.0-0
- Preparing snapshot update to 2.9.0
- Remove icinga2-studio

* Wed Jan 17 2018 Gunnar Beutner <gunnar.beutner@icinga.com> 2.8.1-1
- Update to 2.8.1

* Fri Nov 24 2017 Markus Frosch <markus.frosch@icinga.com> 2.8.0-2
- [SLES] Add systemd limits file
- Add config(noreplace) for the systemd limits file
  (no need to release every OS immediately)
- Update SELinux handling to be compatible to Fedora 27
  (only affecting f27 builds)

* Thu Nov 16 2017 Jean Flach <jean-marcel.flach@icinga.com> 2.8.0-1
- Update to 2.8.0

* Thu Nov 09 2017 Gunnar Beutner <gunnar.beutner@icinga.com> 2.7.2-1
- Update to 2.7.2

* Mon Oct 02 2017 Markus Frosch <markus.frosch@icinga.com> 2.7.1-2
- Fixing systemd limit issues on openSUSE > 42.1

* Thu Sep 21 2017 Michael Friedrich <michael.friedrich@icinga.com> 2.7.1-1
- Update to 2.7.1

* Tue Jun 20 2017 Markus Frosch <markus.frosch@icinga.com> 2.7.0-1
- Update to 2.7.0
